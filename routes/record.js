const express = require('express')
const router = express.Router()
const auth = require('../auth')
const RecordController = require('../controllers/record')

router.get('/', (req, res) => {
	RecordController.getAll().then(records => res.send(records))
})


router.get('/:id', (req, res) => {
	RecordController.getOne({id: req.params.id})
	.then(record => res.send(record))
})


router.post('/add-record', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    RecordController.addRecord(req.body).then(result => res.send(result))
})

router.post('/', auth.verify, (req, res) => {
	const arg = {
		name: req.body.name,
		type: req.body.type,
		amount: req.body.amount,
		description: req.body.description
	}

	RecordController.add(arg).then(result => res.send(result))
})


//update records
router.put('/:id', auth.verify, (req, res)=> {
	const arg = {
		recordId: req.params.id,
		name: req.body.name,
		type: req.body.type,
		amount: req.body.amount,
		description: req.body.description,
		isActive: auth.decode(req.headers.authorization).isActive
	}
	RecordController.update(arg).then(result => res.send(result))
})



//delete records
router.delete('/:id', (req, res) => {
	const arg = {
		recordId: req.params.id,
	}
	RecordController.archive(arg).then(result => res.send(result))
})


router.post('/get-records-by-range', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    RecordController.getRecordsByRange(req.body).then(result => res.send(result))
})


module.exports = router