const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')



router.post('/register', (req, res)=>{
	UserController.register(req.body)
	.then((result) => res.send(result))
})


router.post('/login', (req, res)=>{
	UserController.login(req.body)
	.then(result => res.send(result))
})

//route for getting user info from decoded JWT payload
router.get('/details', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.getDetails({userId: user.id}).then(result => res.send(result))
})

router.get('/ids', (req, res) => {
    UserController.getAllIds().then(result => res.send(result))
})

router.get('/:id', (req, res) => {
	UserController.getDetails({userId: req.params.id})
	.then(result => res.send(result))
})

router.post('/verify-google-id-token', async (req, res)=>{
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})





module.exports = router