const Record = require('../models/record');
const User = require('../models/user');
const moment = require('moment')

//retrieve all records
module.exports.getAll = ()=> {
	return Record
		.find({isActive: true})
		.sort({_id: -1})
		.then(records => records)
}

module.exports.getOne = (params) => {
	return Record.findById(params.id)
	.then((record, err) => {
		if(err) return false
			return record
	})
}


//add record 
module.exports.addRecord = (params) => {

	console.log(params);

	return User.findById(params.userId).then((user, err) => {
		if (err) throw err;

		if (user.records.length !== 0) {
			const currentBalanceBeforeTransaction = user.records[user.records.length - 1].currentBalance;

			if (params.typeName === 'Expense') {
				params.amount = parseFloat(params.amount) * -1;
			}
			
			currentBalance = parseFloat(currentBalanceBeforeTransaction) + parseFloat(params.amount);

		} else {
			currentBalance = params.amount;
		}

		let newRecord = new Record({
			categoryName: params.name,
			categoryType: params.type,
			amount: params.amount,
			description: params.description,
			currentBalance: currentBalance
		});

		return newRecord
			.save()
			.then((record, err) => {
				user.records.push(record);
				
				return user
					.save()
					.then((user, err) => {
						if (err) throw err;

						return user ? true : false;
					});
			});



	});


	// return Record.findById(params.recordId).then(user =>{
	// 	let currentBalance = 0

	// 	if (user.records.length !== 0) {
	// 		const currentBalanceBeforeTransaction = user.records[user.records.length - 1].currentBalance

	// 		if(params.typeName === 'Income') {
	// 			currentBalance = currentBalanceBeforeTransaction + params.amount
	// 		}
	// 		else {
	// 			currentBalance = currentBalanceBeforeTransaction - params.amount
	// 		}
	// 	} else {
	// 		currentBalance = params.amount
	// 	}

	// 	user.records.push({
	// 		categoryName: params.categoryName,
	// 		type: params.typeName,
	// 		amount: params.amount,
	// 		description: params.description,
	// 		currentBalance: currentBalance
	// 	})

	// 	return user.save().then((user, err) => {
	// 		return (err) ? false : true
	// 	})
	// })
}

//update records
module.exports.update = (params) => {
	if(params.isActive){
		return Record.findById(params.recordId)
		.then((record, err)=> {
			console.log(record)
			if(err) return false
				record.name = params.name
				record.type = params.type
				record.amount = params.amount
				record.description = params.description

				return  record.save()
				.then((updatedRecord, err) => {
					console.log(updatedRecord)
					return err ? false : true
				})
		})
	} else{
		return false
	}
}

//archive or delete
module.exports.archive = (params) => {
	return Record.findById(params.recordId)
	.then((record, err) => {
		if(err) return false;

			record.isActive = false;

			console.log(record);

			return record.save()
			.then((updatedRecord, err)=>{
				return err ? false : true
			})
	})
}


module.exports.getRecordsByRange = (params) => {
	return Record.findById(params.recordId).then(user => {
		const recentRecords = user.records.filter((records) => {
			const isSameofAfter = moment(records.createdOn).isSameOrAfter(params.fromDate, 'day')
			const isSameOrBefore = moment(records.createdOn).isSameOrBefore(params.toDate, 'day')

			if (isSameOrAfter && isSameOrBefore) {
				return records
			}
		})
		return recentRecords
	})
}