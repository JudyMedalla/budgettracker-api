const Category = require('../models/category')
const User = require('../models/user')


//get one 
module.exports.getAll = ()=> {
	return Category
		.find({isActive: true})
		.sort({_id: -1})
		.then(categories => categories)
}


//add categories
module.exports.addCategory = (params) => {
	let category = new Category({ 
		'categoryName': params.categoryName, 
		'categoryType': params.categoryType
	});

	return category.save().then((item, err) => {
		return (err) ? false : true
	})
}

