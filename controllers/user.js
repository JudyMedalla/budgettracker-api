const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {OAuth2Client} = require('google-auth-library')
const clientId = '35719299842-9latndrido6hrbsqbf5h064b32887up3.apps.googleusercontent.com'
//register user
module.exports.register = (params) =>{
			console.log(params);
			const user = new User({
				firstName: params.firstName,
				lastName: params.lastName,
				email: params.email,
				password: bcrypt.hashSync(params.password1, 10),
				loginType: 'email'
			})

			return user.save()
				.then((user, err)=>{
					console.log('error data', err);
					console.log('user data', user);
					if(user) return user
						return(err) ? false : true
				})
		}


//login user
module.exports.login = (params)=>{
	return User.findOne({email: params.email})
	.then(user =>{
		if(user === null){
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if(isPasswordMatched){
			return{
				accessToken: auth.createAccessToken(user.toObject())
			}
		}else{
			return false
		}
	})
}


module.exports.getDetails = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.password = undefined
        return user
    })
}


//for getStaticPaths
module.exports.getAllIds = () => {
	return User.find()
	.then((users, err) => {
		if(err) return false
			const ids = users.map(user => user._id)
		return ids
	})
}

//Google login
module.exports.verifyGoogleTokenId = async (tokenId) => {
    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
    if(data.payload.email_verified === true){
        const user = await User.findOne({email: data.payload.email}).exec()
        if(user !== null) {
            if(user.loginType === 'google'){
                return {accessToken: auth.createAccessToken(user.toObject())}
            } else {
                return {error: 'login-type-error'}
            }
        } else {
            const newUser = new User ({
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                email: data.payload.email,
                loginType: 'google'
            })
            return newUser.save().then((user, err) =>{
                console.log(user)
                return {accessToken: auth.createAccessToken(user.toObject())}
            })
        }
    }else{
        return {error: 'google-auth-error'}
    }
}


