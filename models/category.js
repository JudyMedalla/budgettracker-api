const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({

	categoryName:{
		type: String,
		required: [true, 'Category name is required']
	},
	categoryType:{
		type: String,
		required: [true, 'Category type is required']
	},
	isActive: {
		type: Boolean,
		default: true
	}
})

module.exports = mongoose.model('category', categorySchema)